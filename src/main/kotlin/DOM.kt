class DOM {
    companion object {
        private fun h1(node: String) = "<h1>$node</h1>"

        private fun th(node: String) = "<th>$node</th>"

        private fun tr(node: String) = "<tr>$node</tr>"

        private fun table(node: String, id: String = "") = "<table id=\"$id\">$node</table>"

        private fun btn(node: String, href: String, className: String ="", id: String = ""): String {
            return "<a href=\"$href\"><button class=\"$className\" id=\"$id\">$node</button></a>"
        }

        private fun body(node: String) = "<body>$node</body>"

        private fun contactsTable(contacts: List<Contact>): String {
            return table(
                    tr(
                            th("Name") + th("Phone") + th("Delete")
                    ), "contacts"
            )
        }

        private fun css(): String = "<link rel=\"stylesheet\" href=\"style.css\">"

        private fun js(): String = "<script src=\"scripts.js\"></script>"

        private fun head(title: String, node: String) = "<head><title>$title</title>$node</head>"

        private fun html(node: String) = "<!DOCTYPE html>\n<html>$node</html>"

        private fun search() = "<input type=\"text\" id=\"search\" placeholder=\"search\">"

        fun createContactsPage(title: String, contacts: List<Contact>): String {
            return html(head(
                    title, css()
                ) + body(
                    h1(title) + search() + btn("New", "/add", "add") + contactsTable(contacts) + js()
                )
            )
        }

        private fun newContactForm(): String {
            return "<form class=\"new-contact-form\">" +
                    "<input type=\"text\" id=\"form-name\" placeholder=\"Name\"><br>" +
                    "<input type=\"text\" id=\"form-phone\" placeholder=\"Phone\">" +
                    "</form>"
        }

        fun createAddContactPage(): String {
            return html(head(
                    "Add contact", css()
                ) + body(
                        h1("Add contact") + newContactForm()
                                + btn("Add", "#", "add", "new-contact-btn") + js()
                )
            )
        }
    }
}