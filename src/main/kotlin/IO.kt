import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import java.io.File
import java.util.*

class IO {
    companion object {
        fun readFile(path: String): String {
            return File(path).readText()
        }

        fun writeFile(path:String, text: String) {
            File(path).writeText(text)
        }

        fun readContacts(): List<Contact> {
            return Gson().fromJson(readFile(MyServlet.RESOURCE_PATH + "contacts.json"))
        }

        fun writeContacts(contacts: List<Contact>) {
            writeFile(MyServlet.RESOURCE_PATH + "contacts.json", Gson().toJson(contacts))
        }

        fun deleteContact(id: Long) {
            val contacts = IO.readContacts()
            val delElem = contacts.find { it.id == id }
            if (delElem != null) {
                IO.writeContacts(contacts.minus(delElem))
            }
        }

        fun addContact(name: String, phone: String) {
            val secureName = Regex("\\W").replace(name, "")
            val securePhone = Regex("\\D").replace(phone, "")
            val contacts = IO.readContacts()
            IO.writeContacts(contacts.plus(Contact(secureName, securePhone, Date().time)))
        }
    }
}