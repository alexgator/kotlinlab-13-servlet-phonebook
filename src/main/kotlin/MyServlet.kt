import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "Phone Book", value = "/")
class MyServlet : HttpServlet() {

    companion object {
        val RESOURCE_PATH = "/home/user/IdeaProjects/lab-13/src/main/resources/"
    }

    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {

        println(req.requestURI)

        if (req.queryString != null) {
            val delete = req.getParameter("delete")
            val name = req.getParameter("name")
            val phone = req.getParameter("phone")
            if (delete != null) {
                IO.deleteContact(delete.toLong())
            } else if (name != null && phone != null) {
                IO.addContact(name, phone)
            }

            res.sendRedirect("/")

        } else {
            when (req.requestURI) {
                "/"             -> res.writer.print(DOM.createContactsPage("Phone Book", IO.readContacts()))
                "/add"          -> res.writer.print(DOM.createAddContactPage())
                "/style.css"    -> res.writer.print(IO.readFile(RESOURCE_PATH + "style.css"))
                "/scripts.js"   -> res.writer.print(IO.readFile(RESOURCE_PATH + "scripts.js"))
                "/contacts.json"-> res.writer.print(IO.readFile(RESOURCE_PATH + "contacts.json"))
            }
        }
    }
}