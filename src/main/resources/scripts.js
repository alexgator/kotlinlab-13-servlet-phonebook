(function () {
    // Creating listener for New contact button
    let addBtn = document.getElementById("new-contact-btn");

    if (addBtn) {
        addBtn.addEventListener("click", function () {
            let name = document.getElementById("form-name").value;
            let phone = document.getElementById("form-phone").value;
            window.location.replace(`/?name=${name}&phone=${phone}`);
        })
    }

    // Fetch data and fill contacts table
    let table = document.getElementById("contacts");
    let actualContacts = [];

    if (table) {
        fetchContacts().then((contacts) => {
            actualContacts = contacts;
            drawTable(contacts, table)
        })
    }

    function drawTable(contacts, parentTable) {
        while (parentTable.firstChild != parentTable.lastChild) {
            parentTable.removeChild(parentTable.lastChild);
        }
        contacts.forEach((it) => {
            let row = create("tr");
            parentTable.appendChild(row);
            let name = create("td");
            name.innerText = it.name;
            row.appendChild(name);
            let phone = create("td");
            phone.innerText = it.number;
            row.appendChild(phone);
            let del = create("td");
            row.appendChild(del);
            let delBtn = create("button");
            delBtn.classList.add("delete");
            delBtn.innerText = "X";
            delBtn.addEventListener("click", () => {
                fetch(`?delete=${it.id}`)
                    .then((res) => {
                        parentTable.removeChild(row)
                        actualContacts.removeChild(it)
                    })
            });
            del.appendChild(delBtn);
        })
    }

    // Table search
    let search = document.getElementById("search");

    if (search) {
        search.addEventListener("input", (e) => {
            let req = e.target.value;
            if (req == "") {
                drawTable(actualContacts, table)
            } else {
                drawTable(actualContacts.filter((it) => {
                    return it.name.toLowerCase().indexOf(req.toLowerCase()) >= 0
                }), table)
            }
        })
    }

    function create(elem) {
        return document.createElement(elem)
    }

    function fetchContacts() {
        return fetch("contacts.json")
            .then((res) => {
                return res.json()
            })
    }
})();